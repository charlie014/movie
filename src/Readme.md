## Build
Please direct to the src and run
```
npm install - to install all the dependencies
```

## Notes
I was trying to understand the keyword API but couldn't figure what is the keyword_id
so I was unable to fetch them. But I have created the functions to make use of it anytime.

Due to short period of time, I couldn't styling them well, I was trying to make them work
functionally. 

Regarding the Watched, I don't hve API to post the Watch state so I can only make them available
watched instantly, not data persistent. But it can be solved easily when we have database.
