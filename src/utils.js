import * as React from "react";

const API_KEY = "1e448e0dfcdbb565f5d329820065b4d2";

export async function getPopularMovies() {
    const apiUrl = `https://api.themoviedb.org/3/movie/popular?api_key=${API_KEY}&language=en-US&page=1`;
    fetch(apiUrl)
        .then((res) => res.json())
        .then((repos) => {
            setAppState({ repos: repos });
        });
}

