import * as React from "react";
import Button from "@mui/material/Button";
import logo from "./images/logo.png";
import {Link} from "@material-ui/core";

export default function Information () {
    let information = [
        {title: "movify", detail: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque, ducimus, atque. Praesentium suscipit provident explicabo dignissimos nostrum numquam deserunt earum accusantium et fugit."},
        {title: "Tiwtter Feed", detail: "Bacola Grocery Store and Organic Food eCommerce WordPress Theme #grocery #supermarket #organicfood #woocommerce… https://t.co/pTTHRtbHOY"},
        {title: "Tiwtter Feed", detail: "Bacola Grocery Store and Organic Food eCommerce WordPress Theme #grocery #supermarket #organicfood #woocommerce… https://t.co/pTTHRtbHOY"},
        {title: "Tiwtter Feed", detail: "Bacola Grocery Store and Organic Food eCommerce WordPress Theme #grocery #supermarket #organicfood #woocommerce… https://t.co/pTTHRtbHOY"}
    ]

    let links = [
        "About Movify",
        "Contact Us",
        "Testimonials",
        "Blog",
        "Error 404"
    ]
    return (
        <div style={{ backgroundColor: "#3e4555", height: 300}}>
            <div style={{width: "90%", display: "flex", alignContent: "center", margin: "auto"}}>
                <div style={{width: "100%"}}>
                    <img src={logo} style={{width: 80, marginBottom: -25}}/>
                    <h5 style={{marginTop: 30, letterSpacing: 1, lineHeight: 1.5, color: "white", paddingRight: 50}}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque, ducimus, atque. Praesentium
                        suscipit provident explicabo dignissimos nostrum numquam
                        deserunt earum accusantium et fugit.</h5>
                </div>
                <div style={{width: "100%"}}>
                    <h4 style={{color: "white"}}>Twitter Feed</h4>
                    <h5 style={{marginTop: -10, lineHeight: 1.5, color: "white", paddingRight: 50, letterSpacing: 1}}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque, ducimus, atque. Praesentium
                        suscipit provident explicabo dignissimos nostrum numquam
                        deserunt earum accusantium et fugit.</h5>
                </div>
                <div style={{width: "100%"}}>
                    <h4 style={{color: "white", marginBottom: 12}}>Useful Links</h4>
                    <div style={{display: "grid"}}>
                        {links.map(item => {
                            return (
                                <Link style={{color: "white", marginBottom: 10}}>{item}</Link>
                            )
                        })}
                    </div>
                </div>
                <div style={{width: "100%"}}>
                    <h4 style={{color: "white", marginBottom: -10}}>Instagram</h4>
                    <h5 style={{color: "white"}}>Instagram did not return a 200.</h5>
                    <Link style={{color: "white"}}>Follow Us!</Link>
                </div>
            </div>
        </div>
    )
}
