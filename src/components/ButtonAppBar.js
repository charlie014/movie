import * as React from 'react';
import Toolbar from '@mui/material/Toolbar';
import Button from '@mui/material/Button';
import { styled } from '@mui/material/styles';
import logo from "./images/logo.png"
import { makeStyles } from '@mui/styles';
import {Search} from "@mui/icons-material";
import SearchIcon from '@mui/icons-material/Search';
import InputBase from '@mui/material/InputBase';

const useStyles = makeStyles({
    row: {
        backgroundColor: "white"
    },
    column: {
        float: "left",
        width: "33.33%",
        alignContent: "center"
    }
})

const SearchIconWrapper = styled('div')(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: 'inherit',
    '& .MuiInputBase-input': {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)})`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            width: '12ch',
            '&:focus': {
                width: '20ch',
            },
        },
    },
}));


export default function ButtonAppBar() {
    let navbar = [
        "Home",
        "Pages",
        "Movies and TV Shows",
        "Blog",
        "Contact Us"
    ]
    const classes = useStyles();
    return (
        <>
            <div style={{display: "flex", width: "33.33"}}>
                <div style={{width: "100%"}}>
                    <img src={logo} style={{marginTop: 15, width: 150, marginLeft: 30 }}/>
                </div>
                <div style={{margin:"auto", width: "150%"}}>
                            <Toolbar>
                                {navbar.map((item, index) => {
                                    return (
                                        <Button variant="h6" sx={{ flexGrow: 5 }} style={{textAlign: "center", width: ""}} >
                                            {item}
                                        </Button>
                                    )
                                })}
                            </Toolbar>
                </div>
                <div style={{width: "100%", textAlign: "right", marginTop: 20}}>
                    <Search>
                        <SearchIconWrapper>
                            <SearchIcon />
                        </SearchIconWrapper>
                        <StyledInputBase
                            placeholder="Search…"
                            inputProps={{ 'aria-label': 'search' }}
                        />
                    </Search>
                    <Button variant="contained" style={{marginTop: -14, marginLeft: 20, marginRight: 30}}>Login</Button>
                </div>
            </div>
        </>
    );
}
