import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import {useEffect, useState} from "react";
import movieImage from './images/movie.jpeg'

export default function MovieDetails({movie}) {
    const API_KEY = "1e448e0dfcdbb565f5d329820065b4d2";
    const [appState, setAppState] = useState({
        imdb: false,
        data: {},
    });

    const [watch, setWatch] = useState(false)

    useEffect(() => {
        const apiUrl = `https://api.themoviedb.org/3/movie/${movie.id}?api_key=${API_KEY}`;
        fetch(apiUrl)
            .then((res) => res.json())
            .then((data) => {
                setAppState({imdb: true, data: data})
            });
    }, [setAppState]);

    function markWatchMovie() {
        setWatch({
            watch: true
        })
    }

    console.log(movie)
    return (
        <>
            <Card sx={{ maxWidth: 345, height: 600}}>
                <CardMedia
                    component="img"
                    height="140"
                    image = {movieImage}
                    alt="green iguana"
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        {movie.title}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        {movie.overview}
                    </Typography>
                </CardContent>
                <CardActions>
                    {appState.imdb
                        ? <Button size="small" href={appState.data.homepage ? appState.data.homepage : "https://www.imdb.com/"}>Read More</Button>
                    : "" }
                </CardActions>
                <CardActions>
                    {watch ? "Watched" : <Button size="small" onClick={() => markWatchMovie()}>Mark It Watched</Button> }
                </CardActions>
            </Card>
        </>
    );


}
