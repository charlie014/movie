import * as React from 'react';
import { experimentalStyled as styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import MovieDetails from "./MovieDetails";


export default function ResponsiveGrid({movie}) {
    return (
        <div style={{margin: "auto", width: "90%", marginTop: 10, marginBottom: 10}}>
            <Box sx={{ flexGrow: 1 }}>
                <Grid container spacing={{ xs: 2, md: 3 }} columns={{ xs: 4, sm: 8, md: 12 }}>
                    {movie && movie.map((item, index) => (
                        <Grid item xs={2} sm={4} md={4} key={index}>
                            <MovieDetails movie={item}/>
                        </Grid>
                    ))}
                </Grid>
            </Box>
        </div>
    );
}
