import * as React from "react";
import {Button} from "@mui/material";

export default function Footer() {
    let text = [
        "Privacy & Cookies",
        "Terms and Conditions",
        "Legal Disclaimer",
        "Community"
    ]
    return (
        <>
            <div style={{width: 112, height: 47, color: "white", position: "absolute"}}></div>
            <div style={{ backgroundColor: "#3e4555"}}>
                <div style={{width: "90%", alignContent: "center", margin: "auto", display: "flex"}}>
                    <div style={{width: "100%"}}>
                        <div style={{margin: "auto"}}>
                            <div style={{display: "flex"}}>
                                {text.map(item => {
                                    return (
                                        <Button color="inherit" style={{paddingLeft: 0, color: "white", marginRight: 20}}>{item}</Button>
                                    )
                                })}
                            </div>
                        </div>
                    </div>
                    <div style={{width: "100%"}}>
                        <h5 style={{marginTop: 12, textAlign: "center", color: "white"}}>Copyright 2020.KlbTheme . All rights reserved</h5>
                    </div>
                </div>
            </div>
        </>
    )
}
