import * as React from 'react';
import Button from "@mui/material/Button";
import {useState} from "react";
import deco from "./images/deco.jpeg"
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles({
    top: {
        backgroundColor: "antiquewhite",
        marginTop: -20,
        backgroundImage: `url(${deco})`,
        backgroundRepeat: "no-repeat",
        backgroundSize: "100%"
    },
    movie: {
        marginLeft: 150,
        // marginTop: 10
    },
    home: {
        marginLeft: 140,
        display: "flex",
        marginTop: -20
    }
})

export default function Decoration() {
    const API_KEY = "1e448e0dfcdbb565f5d329820065b4d2";
    const [appState, setAppState] = useState({
        movie: {}
    });
    const classes = useStyles();

    async function getMovieByKeyword (keyword_id) {
        const apiUrl = `https://api.themoviedb.org/3/keyword/${keyword_id}?api_key=${API_KEY}`;
        fetch(apiUrl)
            .then((res) => res.json())
            .then((data) => {
                setAppState({ movie: data });
            });
    }
    return (
        <div className={classes.top}>
            <div className={classes.movie}>
                <h2>Movie Grid 3</h2>
            </div>
            <div className={classes.home} style={{}}>
                <Button color="inherit">Home</Button>
                <h5>Movie Grid 3</h5>
            </div>
        </div>
    )
}
