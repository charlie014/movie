import './App.css';
import * as React from "react";
import ButtonAppBar from "./components/ButtonAppBar";
import Decoration from "./components/Decoration";
import Information from "./components/Information";
import Footer from "./components/Footer";
import ResponsiveGrid from "./components/ResponsiveGrid";
import {useEffect, useState} from "react";


function App() {
    const API_KEY = "1e448e0dfcdbb565f5d329820065b4d2";
    const [appState, setAppState] = useState({
        repos: [],
    });

    useEffect(() => {
        const apiUrl = `https://api.themoviedb.org/3/movie/popular?api_key=${API_KEY}&language=en-US&page=1`;
        fetch(apiUrl)
            .then((res) => res.json())
            .then((repos) => {
                setAppState({ repos: repos.results });
            });
    }, [setAppState]);
    return (
      <>
        <ButtonAppBar />
        <Decoration />
        <ResponsiveGrid movie={appState.repos} />
        <Information />
        <Footer />
      </>
  );
}

export default App;




